<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Subscription extends Model
{
    protected $fillable = [
        'name', 'email', 'city', 'country', 'sex', 'interests', 'shopping_online', 'watching_online', 'food_online'
      ];
}
