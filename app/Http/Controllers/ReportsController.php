<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Subscription;

class ReportsController extends Controller
{
    /**
    * Show general report
    *
    * @return \Illuminate\Contracts\Support\Renderable
    */
    public function general()
    {
        // List of subscribers
        $subscribers = Subscription::all();

        // Summary data
        $women = Subscription::where("sex","kobieta")->count();
        $men = Subscription::where("sex","mezczyzna")->count();
        $allCount = $subscribers->count();


        // raw sql example:
        /*
        // List of subscribers
        $subscribers = \DB::select(\DB::raw("SELECT * from subscriptions"));

        // Summary data
        $women = \DB::select("SELECT count(*) as count FROM `subscriptions` WHERE `sex` = 'kobieta'")[0]->count;
        $men = \DB::select("SELECT count(*) as count FROM `subscriptions` WHERE `sex` = 'mezczyzna'")[0]->count;
        $allCount = \DB::select("SELECT count(*) as count from subscriptions")[0]->count;
        */

        return view('reports.general', compact('subscribers','women','men','allCount'));
    }

    /**
    * Show general report
    *
    * @return \Illuminate\Contracts\Support\Renderable
    */
    public function report1()
    {
        // List of subscribers
        $subscribers = Subscription::all();

        // Summary data
        $women = Subscription::where("sex","kobieta")->count();
        $men = Subscription::where("sex","mezczyzna")->count();
        $allCount = $subscribers->count();
        $allCountWarsaw = 0;


        return view('reports.report1', compact('subscribers','women','men','allCount', 'allCountWarsaw'));
    }
}
