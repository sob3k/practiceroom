<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Subscription;

class UtilityController extends Controller
{
    /**
    * Show task1 content
    *
    * @return \Illuminate\Contracts\Support\Renderable
    */
    public function task1()
    {
        return view('tasks.task1');
    }

    /**
    * Show task2 content
    *
    * @return \Illuminate\Contracts\Support\Renderable
    */
    public function task2()
    {
        return view('tasks.task2');
    }

    /**
    * Show task3 content
    *
    * @return \Illuminate\Contracts\Support\Renderable
    */
    public function task3()
    {
        return view('tasks.task3');
    }

    /**
    * Render help page
    *
    * @return \Illuminate\Contracts\Support\Renderable
    */
    public function help()
    {
        return view('help');
    }

}
