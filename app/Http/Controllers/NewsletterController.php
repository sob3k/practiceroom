<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Subscription;

class NewsletterController extends Controller
{
    /**
    * Show the newsletter form
    *
    * @return \Illuminate\Contracts\Support\Renderable
    */
    public function show()
    {
        return view('newsletter');
    }

    /**
    * Show the newsletter form
    *
    * @return \Illuminate\Contracts\Support\Renderable
    */
    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'name' => 'required|max:255',
            'email' => 'required|unique:subscriptions|email',
            'sex' => 'required',
            'country' => 'required',
            'interests' => 'required',
            'shopping_online' => 'required',
            'watching_online' => 'required',
            'food_online' => 'required',
            'rodo' => 'required'
        ]);

        $newSubscription = new Subscription();
        $newSubscription->fill($request->all());
        $newSubscription->save();

        return view('newsletter_subscribed');
    }
}
