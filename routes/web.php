<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/newsletter', 'NewsletterController@show')->name('newsletter.show');
Route::post('/newsletter_zapisz', 'NewsletterController@store')->name('newsletter.store');
Route::get('/raporty/ogolny', 'ReportsController@general')->name('reports.general');
Route::get('/raporty/raport_bFit', 'ReportsController@report1')->name('reports.report1');

Route::get('/zadanie-1', 'UtilityController@task1')->name('task1');
Route::get('/zadanie-2', 'UtilityController@task2')->name('task2');
Route::get('/zadanie-3', 'UtilityController@task3')->name('task3');

Route::get('/pomoc', 'UtilityController@help')->name('help');

