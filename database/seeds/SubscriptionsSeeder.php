<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;

class SubscriptionsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $countries=["af", "ax","al","dz","as","ad","ao","ai","ag","ar","am","aw","au","at","jp","lr","mo","pg","nf","ua","us","vu"];
        $levels=['wcale','rzadko','czesto','glownie'];
        $faker = Faker::create();
        foreach (range(1,100) as $index) {
            $interests=["wiadomosci","muzyka","plotki","sport","technologia","biznes"];
            $interestsSubscriber=[];
            for($i=0;$i<$faker->numberBetween(1,5);$i++){
                array_push($interestsSubscriber,$faker->randomElement($interests));
            }
            $interestsString=implode(',',$interestsSubscriber);

            $sex=$faker->randomElement(['kobieta', 'mezczyzna']);


            DB::table('subscriptions')->insert([
                'name' => $faker->firstName($sex == 'kobieta' ? 'female' : 'male'),
                'email' => $faker->email,
                'sex' => $sex,
                'city' => $faker->randomElement([null,null,null,'Warsaw','Warsaw','Warsaw',$faker->city]),
                'country' => $faker->randomElement(['pl','pl','pl','pl',$faker->randomElement($countries)]),
                'interests' => $interestsString,
                'shopping_online' => $faker->randomElement($levels),
                'watching_online' => $faker->randomElement($levels),
                'food_online' => $faker->randomElement($levels),
                'created_at' => $faker->dateTimeThisYear('now')
                ]);
        }
    }
}
