## Instalacja
Aplikacja oczekuje istniejacej bazy danych MySQL o nazwie 'rekrutacja' oraz użytkownika root bez hasła.
Możliwość alternatywnej konfiguracji poprzez plik ```.env```

Dodatkowo potrzebny będzie composer w celu instalowania dependencies:

[https://getcomposer.org/download/](https://getcomposer.org/download/)


```
composer install
php artisan key:generate
php artisan migrate --seed
```

## Włączenie aplikacji
```
php artisan serve
```
Działająca aplikacja dostępna jest domyślnie pod adresem:


[http://localhost:8000](http://localhost:8000/)
