@extends('layouts.main')

@section('content')
<div class="ui container">
    <div class="ui grid centered">
        <div class="ten wide column">
            <h1>Pomocne informacje</h1>
            <br>
            <h2>Debuggowanie kodu</h2><br>
            wyrenderowanie zmiennej w przejrzystym formacie:<br>
            <div class="ui tiny compact message">
              <p>dump($zmienna)</p>
            </div>
            <br><br>
            wyrenderowanie zmiennej i natychmiastowe zatrzymanie skryptu:<br>
            <div class="ui tiny compact message">
              <p>dd($zmienna)</p>
            </div>

            <h2>Struktura aplikacji</h2>
            <p>
            Aplikacja oparta jest o framework Laravel, który znacznie ułatwia tworzenie funkcjonalności.
            Projekt opiera się o model <strong>MVC</strong>, stąd główne jego składowe znajdują się w poniższych lokalizacjach:<br><br>
            </p>
            <strong>Modele: </strong>
            <div class="ui tiny compact message"><i>app</i></div>
            <br>
            <br>
            <strong>Kontrolery: </strong>
            <div class="ui tiny compact message"><i>app/Http/Controllers</i></div>
            <br>
            <br>
            <strong>Widoki: </strong>
            <div class="ui tiny compact message"><i>resources/views</i></div>
            <br><br><br>
            <h2>Przydatne linki</h2>
            <a href="https://laravel.com/docs/7.x/">Strona główna dokumentacji Laravela</a><br>
            <a href="https://laravel.com/docs/7.x/queries">Obsługa bazy danych</a>

        </div>
    </div>
</div>
@endsection
