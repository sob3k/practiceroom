<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>{{ config('app.name', '') }}</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">

        <!-- Semantic ui includes -->
        <link rel="stylesheet" type="text/css" href="{{ asset('semantic.min.css') }}">
        <script
          src="https://code.jquery.com/jquery-3.1.1.min.js"
          integrity="sha256-hVVnYaiADRTO2PzUGmuLJr8BLUSjGIZsDYGmIJLv2b8="
          crossorigin="anonymous"></script>
        <script src="{{ asset('semantic.min.js') }}"></script>

    </head>
    <body>
        <div class="ui container" style="margin-bottom:2rem;">
            <div class="ui menu">
              <div class="header item">Practice room</div>
              <a class="item" href="{{route('newsletter.show')}}">Newsletter</a>
              <a class="item" href="{{route('reports.report1')}}">Raport - firma bFit</a>

              <div class="right menu">
                <div class="ui topMenu dropdown item" tabindex="0">
                    Zadania
                    <i class="dropdown icon"></i>
                    <div class="menu transition hidden" tabindex="-1">
                      <a href="{{route('task1')}}"><div class="item">Zadanie 1</div></a>
                      <a href="{{route('task2')}}"><div class="item">Zadanie 2</div></a>
                      <a href="{{route('task3')}}"><div class="item">Zadanie 3</div></a>
                      <div class="divider"></div>
                      <a href="{{route('help')}}"><div class="item">Pomocne linki</div></a>
                    </div>
                  </div>
              </div>
            </div>
        </div>
         @yield('content')
    </body>
    <script>
        $('.topMenu.dropdown')
          .dropdown();


    </script>
     @yield('below_body')
</html>
