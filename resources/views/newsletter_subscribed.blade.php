@extends('layouts.main')

@section('content')
<div class="ui container">
    <div class="ui grid centered">
        <div class="ten wide column">
            <div class="ui card" style="width: 100%">
              <div class="content">
                <div class="header">Zapisano na newsletter!</div>
              </div>
              <div class="content">
                <h4 class="ui sub header">Dziękujemy</h4>
                <div class="ui small feed">
                  <div class="event">
                    <div class="content">
                      <div class="summary">
                         Dziękujemy za zapisanie się na nasz newsletter. Postaramy się dostarczyć Ci wszelkich treści,
                         którymi będziesz zainteresowany, oraz ofert, które wpasowują się w Twój profil. Miłego dnia!
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
        </div>
    </div>
</div>
@endsection
