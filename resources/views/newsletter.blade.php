@extends('layouts.main')

@section('content')
<div class="ui container">
    <div class="ui grid centered">
        <div class="ten wide column">
            <h1>Personalizowany newsletter</h1>
            @if ($errors->any())
                <div class="ui error message">
                    <i class="close icon"></i>
                    <div class="header">
                        Wykryliśmy następujące błędy w formularzu
                    </div>
                    <ul class="list">
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif

            <form action="{{route('newsletter.store')}}" method="post" class="ui form">
                @csrf
                <h4 class="ui dividing header">Informacje podstawowe</h4>
                <div class="field required">
                    <label>Imię</label>
                    <div class="field">
                        <input type="text" name="name" placeholder="Imię" value="{{ old('name') }}">
                    </div>
                </div>
                <div class="field required">
                    <label>Adres email</label>
                    <div class="field">
                        <input type="email" name="email" placeholder="@" value="{{ old('email') }}">
                    </div>
                </div>
                <div class="two fields">
                    <div class="field">
                        <label>Miasto</label>
                        <input type="text" name="city" placeholder="Miasto">
                    </div>
                    @include('partials.newsletter.country')
                </div>
                <div class="field">
                    <label for="sex">Wybierz swoją płeć</label>
                    <div class="fields">
                        <div class="field">
                            <div class="ui radio checkbox">
                                <input type="radio" value="kobieta" @if(old('sex') == "kobieta") checked @endif name="sex" checked="" tabindex="0" class="hidden">
                                <label>Kobieta</label>
                            </div>
                        </div>
                        <div class="field">
                            <div class="ui radio checkbox">
                                <input type="radio" value="mezczyzna" @if(old('sex') == "mezczyzna") checked @endif name="sex" tabindex="0" class="hidden">
                                <label>Mężczyzna</label>
                            </div>
                        </div>
                    </div>
                </div>
                <h4 class="ui dividing header">Rozszerzone informacje</h4>
                <br>
                <div class="field required">
                <label>Interesujące Cię typy treści</label>
                <div class="ui fluid multiple search selection dropdown" id="interests">
                    <input type="hidden" name="interests">
                    <i class="dropdown icon"></i>
                    <div class="default text">Wybierz kilka</div>
                    <div class="menu">
                        <div class="item" data-value="wiadomosci" data-text="Wiadomości">
                          Wiadomości
                        </div>
                        <div class="item" data-value="muzyka" data-text="Muzyka">
                          Muzyka
                        </div>
                        <div class="item" data-value="technologia" data-text="Technologia">
                          Technologia
                        </div>
                        <div class="item" data-value="sport" data-text="Sport">
                          Sport
                        </div>
                        <div class="item" data-value="plotki" data-text="Plotki">
                          Plotki
                        </div>
                        <div class="item" data-value="biznes" data-text="Biznes">
                          Biznes
                        </div>
                    </div>
                </div>
                </div>
                <br>
                <div class="field">
                    <label for="shopping_online">Jak często robisz zakupy przez internet?</label>
                    <div class="inline fields">
                        <div class="field">
                            <div class="ui radio checkbox">
                                <input type="radio" value="wcale" @if(old('shopping_online') == "wcale") checked @endif name="shopping_online" checked="" tabindex="0" class="hidden">
                                <label>Wcale</label>
                            </div>
                        </div>
                        <div class="field">
                            <div class="ui radio checkbox">
                                <input type="radio" value="rzadko" @if(old('shopping_online') == "rzadko") checked @endif name="shopping_online" tabindex="0" class="hidden">
                                <label>Rzadko</label>
                            </div>
                        </div>
                        <div class="field">
                            <div class="ui radio checkbox">
                                <input type="radio" value="czesto" @if(old('shopping_online') == "czesto") checked @endif name="shopping_online" tabindex="0" class="hidden">
                                <label>Często</label>
                            </div>
                        </div>
                        <div class="field">
                            <div class="ui radio checkbox">
                                <input type="radio" value="glownie" @if(old('shopping_online') == "glownie") checked @endif  name="shopping_online" tabindex="0" class="hidden">
                                <label>Głównie przez internet</label>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="field">
                    <label for="watching_online">Jak często oglądasz filmy/seriale przez internet?</label>
                    <div class="inline fields">
                        <div class="field">
                            <div class="ui radio checkbox">
                                <input type="radio" value="wcale" @if(old('shopping_online') == "wcale") checked @endif name="shopping_online" checked="" tabindex="0" class="hidden">
                                <label>Wcale</label>
                            </div>
                        </div>
                        <div class="field">
                            <div class="ui radio checkbox">
                                <input type="radio" value="rzadko" @if(old('shopping_online') == "rzadko") checked @endif name="shopping_online" tabindex="0" class="hidden">
                                <label>Rzadko</label>
                            </div>
                        </div>
                        <div class="field">
                            <div class="ui radio checkbox">
                                <input type="radio" value="czesto" @if(old('shopping_online') == "czesto") checked @endif name="shopping_online" tabindex="0" class="hidden">
                                <label>Często</label>
                            </div>
                        </div>
                        <div class="field">
                            <div class="ui radio checkbox">
                                <input type="radio" value="glownie" @if(old('shopping_online') == "glownie") checked @endif name="shopping_online" tabindex="0" class="hidden">
                                <label>Głównie przez internet</label>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="field">
                <label for="food_online">Jak często zamawiasz jedzenie przez internet?</label>
                <div class="inline fields">
                    <div class="field">
                        <div class="ui radio checkbox">
                            <input type="radio" value="wcale" @if(old('food_online') == "wcale") checked @endif name="food_online" checked="" tabindex="0" class="hidden">
                            <label>Wcale</label>
                        </div>
                    </div>
                    <div class="field">
                        <div class="ui radio checkbox">
                            <input type="radio" value="rzadko" @if(old('food_online') == "rzadko") checked @endif name="food_online" tabindex="0" class="hidden">
                            <label>Rzadko</label>
                        </div>
                    </div>
                    <div class="field">
                        <div class="ui radio checkbox">
                            <input type="radio" value="czesto" @if(old('food_online') == "czesto") checked @endif name="food_online" tabindex="0" class="hidden">
                            <label>Często</label>
                        </div>
                    </div>
                    <div class="field">
                        <div class="ui radio checkbox">
                            <input type="radio" value="glownie" @if(old('food_online') == "glownie") checked @endif name="food_online" tabindex="0" class="hidden">
                            <label>Głównie przez internet</label>
                        </div>
                    </div>
                </div>
                </div>
                <br><br>
                <div class="required inline field">
                 <div class="ui checkbox">
                    <input type="checkbox" tabindex="0" class="hidden" name="rodo">
                    <label>
                    Zgadzam się na przetwarzanie moich danych osobowych przez spółkę Umbrella
                    spółka z o.o. z siedzibą w Warszawie ul. Domaniewska 43, w celu marketingowym.
                    <br><br>
                    Administratorem Twoich danych jest Umbrella spółka z o.o. z siedzibą w Warszawie ul. Domaniewska 43,
                    wpisanej do Centralnej Ewidencji i Informacji o Działalności Gospodarczej, pod numerem NIP:
                    4974639807, REGON: 936339060. W sprawach związanych z przetwarzaniem i ochroną danych osobowych
                    wyznaczony został punkt kontaktowy w postaci adresu e-mail: rodo@umbrella.com.pl Kontakt we wskazanych sprawach
                    możliwy jest także w postaci tradycyjnej korespondencji na adres administratora z dopiskiem: „Ochrona Danych Osobowych”
                    oraz osobiście pod adresem 03-123 Warszawa, ul. Domaniewska 43.
                    </label>
                 </div>
                </div>
                <button type="submit" class="ui button" tabindex="0">Zapisz mnie na newsletter</button>
            </form>
        </div>
    </div>
</div>
@endsection

@section('below_body')
<script>
    $('#country')
      .dropdown('set selected', '{{old('country')}}');

    $('#interests')
      .dropdown('set selected', [
        @foreach(explode(',', old('interests')) as $item)
            '{{$item}}',
        @endforeach
      ]);

    $('.ui.radio.checkbox')
      .checkbox();

    $('.ui.checkbox')
      .checkbox();
</script>
@endsection
