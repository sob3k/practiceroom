@extends('layouts.main')

@section('content')
<div class="ui container">
    <div class="ui grid centered">
        <div class="row">
            <div class="column">
                <h1>Raport dla firmy bFit</h1>
                <h2>Sumaryczne wartości</h2>

                <table class="ui very basic collapsing celled table">
                  <thead>
                    <tr><th>Typ danych</th>
                    <th>Wartość</th>
                  </tr></thead>
                  <tbody>
                    <tr>
                      <td>
                        <h4 class="ui header">
                          <div class="content">
                            Osoby zainteresowane sportem i często/głównie zamawiające jedzenie online - łącznie
                          </div>
                        </div>
                      </h4></td>
                      <td>
                        {{$allCount}}
                      </td>
                    </tr>
                    <tr>
                      <td>
                        <h4 class="ui header">
                          <div class="content">
                            Kobiety spełniające kryteria
                          </div>
                        </div>
                      </h4></td>
                      <td>
                        {{$women}}
                      </td>
                    </tr>
                    <tr>
                      <td>
                        <h4 class="ui header">
                          <div class="content">
                            Mężczyźni spełniający kryteria
                          </div>
                        </div>
                      </h4></td>
                      <td>
                        {{$men}}
                      </td>
                    </tr>
                    <tr>
                      <td>
                        <h4 class="ui header">
                          <div class="content">
                            Osoby z Warszawy spełniające kryteria
                          </div>
                        </div>
                      </h4></td>
                      <td>
                        {{$allCountWarsaw}}
                      </td>
                    </tr>
                  </tbody>
                </table>
            </div>
        </div>
        <div class="row">
            <div class="column">
                <h2>Lista subskrybentów</h2>
                <table class="ui celled table">
                  <thead>
                    <tr><th>Imię</th>
                    <th>Email</th>
                    <th>Miasto</th>
                    <th>Kraj</th>
                    <th>Płeć</th>
                    <th>Zainteresowania</th>
                    <th>Zakupy online</th>
                    <th>Oglądanie online</th>
                    <th>Jedzenie online</th>
                    <th>Dołączył</th>
                  </tr></thead>
                  <tbody>
                    @foreach($subscribers as $subscriber)
                    <tr>
                      <td data-label="Imię">{{$subscriber->name}}</td>
                      <td data-label="Email">{{$subscriber->email}}</td>
                      <td data-label="Miasto">{{$subscriber->city}}</td>
                      <td data-label="Kraj">{{$subscriber->country}}</td>
                      <td data-label="Płeć">{{$subscriber->sex}}</td>
                      <td data-label="Zainteresowania">{{$subscriber->interests}}</td>
                      <td data-label="Zakupy online">{{$subscriber->shopping_online}}</td>
                      <td data-label="Filmy/seriale online">{{$subscriber->watching_online}}</td>
                      <td data-label="Jedzenie zamawiane online">{{$subscriber->food_online}}</td>
                      <td data-label="Dołączył">{{$subscriber->created_at}}</td>
                    </tr>
                    @endforeach
                  </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection
