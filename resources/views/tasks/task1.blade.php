@extends('layouts.main')

@section('content')
<div class="ui container">
    <div class="ui grid centered">
        <div class="ten wide column">
            <div class="ui icon message">
              <i class="inbox icon" style="bottom:140px;position:relative"></i>
              <div class="content">
                <div class="header">
                  Problem z nowym formularzem newslettera - prośba o poprawkę
                </div>
                <p>
                    Witaj!
                    <br><br>
                    Miło mi powitać Cię na stanowisku <i>Junior Engineer'a</i>! :) Wiem, że dopiero zaczynasz swoją przygodę w naszej firmie,
                    ale jestem zmuszona od razu prosić Cię o wsparcie. Jeden z programistów rozpoczął prace nad formularzem zgłoszeniowym
                    do naszego nowego newslettera, niestety z powodów zdrowotnych będzie niedostępny przez kolejne kilka tygodni, a terminy nas gonią. Dlatego bardzo liczę na Twoją pomoc!
                    <br><br>
                    <strong>
                    Problem dotyczy części, w której zbieramy informacje o nawykach związanych z usługami online - sekcja "rozszerzone informacje". Zaznaczenie jednej z opcji
                    powoduje czasem utracenie wartości wybranej wcześniej. Dodatkowo przy próbie wysłania zgłoszenia dostaję błędy związane z walidacją.
                    </strong>
                    <br><br>
                    Rzuć proszę na to okiem, formularz znajdziesz pod <a href="{{route('newsletter.show')}}">tym adresem</a>.<br>
                    <br>
                    Adam Nowak
                    <br>
                    <i>Product Owner</i>

                </p>
              </div>
            </div>
        </div>
    </div>
</div>
@endsection
