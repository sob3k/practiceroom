@extends('layouts.main')

@section('content')
<div class="ui container">
    <div class="ui grid centered">
        <div class="ten wide column">
            <div class="ui icon message">
              <i class="inbox icon" style="bottom:140px;position:relative"></i>
              <div class="content">
                <div class="header">
                  Dodanie nowego raportu z danymi dla klienta
                </div>
                <p>
                    Cześć
                    <br><br>
                    Dostaliśmy informacje od naszego działu marketingu o podjęciu współpracy z nowym klientem, który zajmuje się
                    cateringiem dietetycznym - firma <strong>bFit</strong>. Klient oprócz standardowych kampanii reklamowych liczy na pomoc w dotarciu do użytkowników,
                    którzy potencjalnie wpasowują się w profil osób, które najchętniej sięgają po jego usługi.
                    <br><br>
                    Obecnie posiadamy raport, który dostarcza nam podstawowych informacji o zapisanych na newsletter użytkownikach.<br>
                    Możesz go znaleźć tu: <a href="{{route('reports.general')}}">raport</a>
                    <br><br>
                    <strong>
                    Chcąc jednak realnie ocenić potencjał naszej bazy subskrybentów potrzebujemy raportu, który będzie zawierał:</strong><br>
                    - listę osób z Polski, którzy są zainteresowani sportem, oraz <i>często</i> badź <i>głównie</i> zamawiają jedzenie online<br>
                    - ogólną ilość osób spełniających powyższe kryteria<br>
                    - liczbę kobiet, które spełniają powyższe kryteria<br>
                    - liczbę mężczyzna, którzy spełniają powyższe kryteria<br>
                    - liczbę osób z Warszawy, która spełnia powyższe kryteria<br>
                    <br>
                    Prośba, aby raport był dostępny pod <a href="{{route('reports.report1')}}">takim adresem</a>.<br><br>
                    Adam Nowak
                    <br>
                    <i>Product Owner</i>

                </p>
              </div>
            </div>
        </div>
    </div>
</div>
@endsection
