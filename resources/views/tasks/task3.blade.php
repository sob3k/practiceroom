@extends('layouts.main')

@section('content')
<div class="ui container">
    <div class="ui grid centered">
        <div class="ten wide column">
            <div class="ui icon message">
              <i class="inbox icon" style="bottom:140px;position:relative"></i>
              <div class="content">
                <div class="header">
                  Rozszerzenie aplikacji - dodanie bazy produktów i powiązanie z kategoriami
                </div>
                <p>
                    Hej :)
                    <br><br>
                    W wyniku długotrwałych negocjacji z wieloma firmami, które z nami współpracują narodził się pomysł, aby
                    wykorzystać potencjał informacji na temat osób zapisanych na nasz newsletter. Mamy wstępny plan, aby na naszej stronie
                    pojawiła się sekcja z produktami. Będą one linkować do miejsc, w których użytkownicy mogą je kupić.
                    Dzięki temu, że są naszymi subskrybentami otrzymają oni specjalne kody rabatowe - bardzo atrakcyjne oferty na ich zakup.
                    <br><br>
                    Potrzebuję informacji o tym jakie są możliwości roszerzenia naszej aplikacji. Z funkcjonalnego punktu widzenia potrzebujemy:
                    <br><br>
                    <strong>
                    - dodać produkty posiadające szereg atrybutów do aplikacji<br>
                    - stworzyć podstronę z listą produktów, oraz możlwością filtrowania ich<br>
                    - dać możliwość kategoryzowania produktów<br>
                    - stworzyć raport łączący osoby zainteresowane danymi kategoriami z produktami będącymi w tych kategoriach<br>
                    <br>
                    </strong>
                    Będę wdzięczny za oszacowanie nakładów pracy, ile mniej więcej czasu będziemy potrzebować na zrealizowanie takich funkcjonalności.<br><br>
                    Adam Nowak
                    <br>
                    <i>Product Owner</i>

                </p>
              </div>
            </div>
        </div>
    </div>
</div>
@endsection
