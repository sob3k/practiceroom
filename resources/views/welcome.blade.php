@extends('layouts.main')

@section('content')
<div class="ui container">
    <div class="ui grid centered">
        <div class="ten wide column">
            <h1>Instalacja przebiegła poprawnie, wszystko jest gotowe do działania. :)<br><br>Powodzenia!</h1>
        </div>
    </div>
</div>
@endsection
